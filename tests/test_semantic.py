from sheep8.ast import WhenFunc, WhenType
from sheep8.environ import WhenVarSym, WhenFunSym
from sheep8.errors import CompilationError
from sheep8.types import void, byte

from .utils import (
    arg_file, expect_ok, expect_error,
    SEMANTIC
)

when_void = WhenType(name='void')
when_byte = WhenType(name='byte')

def check_pervasive_types(types):
    assert set(types.table.keys()) == {'void', 'byte'}

@expect_error(arg_file('empty_file.sh8'), SEMANTIC, CompilationError)
def test_empty_file(e):
    assert e.args == (
        'An entry point (`main` function) is missing', None)

@expect_ok(arg_file('empty_func.sh8'), SEMANTIC)
def test_empty_func(program):
    assert program.tree == [
        WhenFunc(name='main', rettype=when_void, args=[], body=[])]
    assert program.symbols.table == {
        'main': WhenFunSym(name='main', rettype=void, argtypes=[])}
    check_pervasive_types(program.types)

@expect_error(arg_file('main_badarg.sh8'), SEMANTIC, CompilationError)
def test_main_badarg(e):
    assert e.args[0] == (
        'Entry point must have no argument and must return nothing (void)')
    assert e.args[1]

@expect_error(arg_file('main_badrettype.sh8'), SEMANTIC, CompilationError)
def test_main_badrettype(e):
    assert e.args[0] == (
        'Entry point must have no argument and must return nothing (void)')
    assert e.args[1]

@expect_ok(arg_file('func_arguments.sh8'), SEMANTIC)
def test_func_signatures(program):
    assert program.symbols.lookup('add') == WhenFunSym(
        name='add', rettype=byte, argtypes=[byte, byte])
    assert program.symbols.lookup('neg') == WhenFunSym(
        name='neg', rettype=byte, argtypes=[byte])
    assert program.symbols.lookup('main') == WhenFunSym(
        name='main', rettype=void, argtypes=[])

@expect_ok(arg_file('affect_simple.sh8'), SEMANTIC)
def test_args_usage(program):
    assert 'a' not in program.symbols.table
    assert 'b' not in program.symbols.table

@expect_error(arg_file('func_arg_void.sh8'), SEMANTIC, CompilationError)
def test_fun_arg_void(e):
    assert e.args[0] == 'Function arguments cannot be void'
    assert e.args[1]

@expect_error(arg_file('vardecl_void.sh8'), SEMANTIC, CompilationError)
def test_vardecl_void(e):
    assert e.args[0] == 'Variables cannot be void'
    assert e.args[1]

@expect_ok(arg_file('vardecl_samearg.sh8'), SEMANTIC)
def test_vardecl_samearg(program):
    pass

@expect_error(arg_file('vardecl_samevar.sh8'), SEMANTIC, CompilationError)
def test_vardecl_samevar(e):
    assert ' '.join(e.args[0].split()[:4]) == 'Already defined variable x'
    assert e.args[1]

@expect_ok(arg_file('vardecl_samefunc.sh8'), SEMANTIC)
def test_vardecl_samefunc(program):
    pass

@expect_ok(arg_file('vardecl_sametype.sh8'), SEMANTIC)
def test_vardecl_sametype(program):
    pass

@expect_ok(arg_file('func_sametype.sh8'), SEMANTIC)
def test_func_sametype(program):
    pass

@expect_error(arg_file('affect_expr.sh8'), SEMANTIC, CompilationError)
def test_affect_not_lvalue(e):
    assert e.args[0] == 'Target is not a l-value'
    assert e.args[1]

@expect_error(arg_file('vardecl_affectearly.sh8'), SEMANTIC, CompilationError)
def test_vardecl_affectearly(e):
    assert e.args[0] == 'Undefined variable: x'
    assert e.args[1]

@expect_error(arg_file('vardecl_unknowntype.sh8'), SEMANTIC, CompilationError)
def test_unknown_type(e):
    assert e.args[0] == 'Unknown typename: unknowntype'
    assert e.args[1]

@expect_error(arg_file('undefined_var.sh8'), SEMANTIC, CompilationError)
def test_undefined_var(e):
    assert e.args[0] == 'Undefined variable: i'
    assert e.args[1]

@expect_error(arg_file('undefined_func.sh8'), SEMANTIC, CompilationError)
def test_undefined_func(e):
    assert e.args[0] == 'Undefined function: unknown_function'
    assert e.args[1]

@expect_error(arg_file('func_firstclass.sh8'), SEMANTIC, CompilationError)
def test_func_firstclass(e):
    assert e.args[0] == (
        'Functions are not first-class values,'
        ' you can only call them')
    assert e.args[1]

@expect_error(arg_file('func_neg_integer.sh8'), SEMANTIC, CompilationError)
def test_neg_byte(e):
    assert e.args[0] == 'Negative bytes are not handled'
    assert e.args[1]

@expect_error(arg_file('neg_void.sh8'), SEMANTIC, CompilationError)
def test_neg_void(e):
    assert e.args[0] == 'Only bytes have an opposite (operand is void)'
    assert e.args[1]

@expect_ok(arg_file('binops.sh8'), SEMANTIC)
def test_binops(program):
    pass

@expect_error(arg_file('binop_void.sh8'), SEMANTIC, CompilationError)
def test_binop_void(e):
    assert e.args[0] == (
        'Integer operations can only work with bytes'
        ' (left operand is void)')
    assert e.args[1]

@expect_error(arg_file('affect_badtype.sh8'), SEMANTIC, CompilationError)
def test_affect_badtype(e):
    assert e.args[0] == 'Cannot assign a void to a byte variable'
    assert e.args[1]

@expect_error(arg_file('call_badtype.sh8'), SEMANTIC, CompilationError)
def test_call_badtype(e):
    assert e.args[0] == 'neg requires a byte for argument 1 (void given)'
    assert e.args[1]

@expect_error(arg_file('func_badrettype.sh8'), SEMANTIC, CompilationError)
def test_func_badrettype(e):
    assert e.args[0] == 'Cannot return a byte, main expects a void instead'
    assert e.args[1]

@expect_error(arg_file('func_samefun.sh8'), SEMANTIC, CompilationError)
def test_func_samefun(e):
    assert ' '.join(e.args[0].split()[:4]) == 'Already defined symbol main'
    assert e.args[1]

@expect_ok(arg_file('func_unordered.sh8'), SEMANTIC)
def test_func_unordered(program):
    pass

@expect_ok(arg_file('if_single_empty.sh8'), SEMANTIC)
def test_if_empty(program):
    pass

@expect_ok(arg_file('if_else_empty.sh8'), SEMANTIC)
def test_if_else_empty(program):
    pass

@expect_ok(arg_file('if_elif_else_simple.sh8'), SEMANTIC)
def test_if_elif_else_empty(program):
    pass

@expect_ok(arg_file('cond_to_value.sh8'), SEMANTIC)
def test_cond_to_value(program):
    pass

@expect_ok(arg_file('cond_to_cond.sh8'), SEMANTIC)
def test_cond_to_cond(program):
    pass

@expect_error(arg_file('func_rec_direct.sh8'), SEMANTIC, CompilationError)
def test_func_recursive_direct(e):
    assert e.args[0] == 'Recursion is not allowed: main calls itself'
    assert e.args[1]

@expect_error(arg_file('func_rec_indirect.sh8'), SEMANTIC, CompilationError)
def test_func_recursive_indirect(e):
    assert e.args[0] == (
        'Recursion is not allowed: f4 calls f1, but:\n'
        '    line 2, column 5: f1 calls f2\n'
        '    line 6, column 5: f2 calls f3')
    assert e.args[1]

@expect_ok(arg_file('while_empty.sh8'), SEMANTIC)
def test_while_empty(program):
    pass

@expect_ok(arg_file('while_cond_complex.sh8'), SEMANTIC)
def test_while_cond_complex(program):
    pass

@expect_ok(arg_file('do_while_empty.sh8'), SEMANTIC)
def test_do_while_empty(program):
    pass

@expect_ok(arg_file('do_while_simple.sh8'), SEMANTIC)
def test_do_while_simple(program):
    pass

@expect_error(arg_file('if_cond_void.sh8'), SEMANTIC, CompilationError)
def test_if_cond_void(e):
    print(e.args)
    assert e.args[0] == 'A byte is required for the condition (void given)'
    assert e.args[1]

@expect_error(arg_file('while_cond_void.sh8'), SEMANTIC, CompilationError)
def test_while_cond_void(e):
    print(e.args)
    assert e.args[0] == 'A byte is required for the condition (void given)'
    assert e.args[1]

@expect_error(arg_file('do_while_cond_void.sh8'), SEMANTIC, CompilationError)
def test_do_while_cond_void(e):
    print(e.args)
    assert e.args[0] == 'A byte is required for the condition (void given)'
    assert e.args[1]
