from sheep8.utils import align, record, or_all, unzip


Ident, WhenIdent = record('Identifier', 'location name')
Func, WhenFunc = record('Function', 'location name args body')

f_simple = Func('location_1', 'f_simple', [], [])
f_nested = Func('location_1', 'f1',
    [Ident('location_2', 'arg1'), Ident('location_3', 'arg2')],
    []
)

def test_simple_eq():
    f2 = Func('location_1', 'f_simple', [], [])
    assert f_simple == f2

def test_simple_neq():
    assert f_simple != Func('location_1', 'f2', [], [])

def test_nested_eq():
    assert f_nested == Func('location_1', 'f1',
        [Ident('location_2', 'arg1'), Ident('location_3', 'arg2')],
        []
    )

def test_nested_neq():
    assert f_nested != Func('location_1', 'f1',
        [Ident('location_2', 'arg1'), Ident('location_3', 'arg3')],
        []
    )

def test_match_simple_single_ok():
    assert f_simple == WhenFunc(body=[])

def test_match_simple_dual_ok():
    assert f_simple == WhenFunc(body=[], args=[])

def test_match_nested_ok():
    assert f_nested == WhenFunc(args=[
        Ident('location_2', 'arg1'), Ident('location_3', 'arg2')
    ])

def test_match_nested_nok():
    assert f_nested != WhenFunc(args=[
        Ident('location_2', 'arg1'), Ident('location_4', 'arg2')
    ])

def test_or_all_none():
    try:
        or_all(())
    except ValueError:
        pass
    else:
        assert False, 'or_all was supposed to raise a ValueError'

def test_or_all_one():
    assert or_all((1, )) == 1

def test_or_all_many():
    assert or_all(1 << i for i in range(8)) == 0xff

def test_align():
    assert [align(i, 4) for i in range(20)] == [
        0,  4,  4,  4,  # 4
        4,  8,  8,  8,  # 8
        8,  12, 12, 12, # 12
        12, 16, 16, 16,  # 16
        16, 20, 20, 20,  # 20
    ]

def test_unzip():
    assert unzip([(1, 2), (3, 4), (5, 6)], 2) == (
        [1, 3, 5], [2, 4, 6])
