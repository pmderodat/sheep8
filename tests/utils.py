from functools import wraps
import os.path

from funcparserlib.parser import ParserError
from lexy import LexingError

from sheep8 import ast
from sheep8.canonical import Canonicaliser
from sheep8.chip8 import Chip8Frame
from sheep8.errors import CompilationError
from sheep8.parser import lex_and_parse
from sheep8.semantic import Semantic


LEX_PARSE, SEMANTIC, CANONICAL = range(3)


def run(argv, last_step):
    '''
    Run the compiler stopping at a given stage. What is returned depends on the
    stage. See the code for more information.
    '''

    with open(argv[1], 'r') as source:
        tree = lex_and_parse(source)
    if last_step == LEX_PARSE:
        return (tree, )

    semantic = Semantic(Chip8Frame)
    program = semantic.process(tree)
    # TODO: since there is no test for canonicalisation, we use semantic pass
    # tests to at least check that further steps raise no error.
    canonicaliser = Canonicaliser(program)
    program.map_proc(canonicaliser.canonicalise)

    if last_step == SEMANTIC:
        return (program, )
    else:
        raise ValueError('Invalid stage: {}'.format(last_step))

def expect_ok(argv, last_step):
    def decorator(func):
        @wraps(func)
        def wrapper():
            return func(*run(['sheep8'] + argv, last_step))
        return wrapper
    return decorator

def expect_error(argv, last_step, exn):
    def decorator(func):
        @wraps(func)
        def wrapper():
            try:
                result = run(['sheep8'] + argv, last_step)
            except exn as e:
                return func(e)
            assert False, (
                'I expected an error ({}) but everything went well'.format(
                    exn))
        return wrapper
    return decorator

def arg_file(filename):
    tests_directory = os.path.dirname(__file__)
    return [os.path.join(tests_directory, filename)]
