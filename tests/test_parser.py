from lexy import LexingError
from funcparserlib.parser import ParserError

from sheep8.ast import *

from .utils import (
    arg_file, expect_ok, expect_error,
    LEX_PARSE
)

void = WhenType(name='void')
byte = WhenType(name='byte')

def o(op, op1, op2):
    op1, op2 = [
        (WhenInt(value=oper)
        if isinstance(oper, int) else
            (WhenIdent(name=oper)
            if isinstance(oper, str) else
            oper))
        for oper in (op1, op2)]
    return WhenBinOp(op=op, left=op1, right=op2)

@expect_ok(arg_file('empty_file.sh8'), LEX_PARSE)
def test_empty_file(tree):
    assert tree == []

@expect_ok(arg_file('empty_func.sh8'), LEX_PARSE)
def test_empty_func(tree):
    assert tree == [
        WhenFunc(name='main', rettype=void, args=[], body=[])]

@expect_ok(arg_file('empty_three_funcs.sh8'), LEX_PARSE)
def test_empty_three_funcs(tree):
    assert tree == [
        WhenFunc(name='main1', rettype=void, args=[], body=[]),
        WhenFunc(name='main2', rettype=void, args=[], body=[]),
        WhenFunc(name='main', rettype=void, args=[], body=[])
    ]

@expect_ok(arg_file('comment.sh8'), LEX_PARSE)
def test_comment(tree):
    pass

@expect_ok(arg_file('func_integer.sh8'), LEX_PARSE)
def test_func_integer(tree):
    assert tree[0].body == [WhenExpr(expr=WhenInt(value=1))]

@expect_ok(arg_file('func_neg_integer.sh8'), LEX_PARSE)
def test_func_neg_integer(tree):
    assert tree[0].body == [WhenExpr(expr=(WhenInt(value=-1)))]

@expect_ok(arg_file('func_arguments.sh8'), LEX_PARSE)
def test_func_arguments(tree):
    assert tree[:-1] == [
        WhenFunc(name='add', rettype=byte, args=[
            WhenFuncArg(type=byte, name='op1'),
            WhenFuncArg(type=byte, name='op2'),
        ]),
        WhenFunc(name='neg', rettype=byte, args=[WhenFuncArg(type=byte, name='op')]),
    ]

@expect_ok(arg_file('func_return.sh8'), LEX_PARSE)
def test_func_return(tree):
    assert tree[:-1] == [WhenFunc(name='forty_two', rettype=byte, body=[
        WhenRet(expr=WhenInt(value=42))
    ])]

@expect_ok(arg_file('binops.sh8'), LEX_PARSE)
def test_binops(tree):
    assert [stmt.expr for stmt in tree[0].body if stmt == WhenExpr()] == [
        o(op, 'op1', 'op2') for op in '+ - * | & or and'.split()]

@expect_ok(arg_file('binops_priority.sh8'), LEX_PARSE)
def test_binops_priority(tree):
    assert tree[0].body == [WhenExpr(expr=
        o('or', 1, o('and', 2,
        o('==', 3, o('!=', 4,
        o('<', 5, o('>', 6,
        o('<=', 7, o('>=', 8,
        o('+', 9, o('-', 10,
        o('*', 11, o('|', 12,
        o('&', 13, o('<<', 14,
        o('>>', 15, 16)
        )) )) )) )) )) )) )) ),
        WhenExpr(expr=o('or',
            o('and', o('>=', o('-',
            o('*', o('|', o('&', o('>>', 1, o('<<', 2, 3)), 4), 5), 6),
            o('+', 7, 8)),
            o('<=', 9, o('>', 10, o('<', 11, o('!=', 12, o('==', 13, 14))))),
            ), 15,), 16)
    )]

@expect_ok(arg_file('preops.sh8'), LEX_PARSE)
def test_preops(tree):
    assert [stmt.expr for stmt in tree[0].body if stmt==WhenExpr()] == [
        WhenPreOp(op=op, expr=WhenIdent(name='op'))
        for op in '- not ~'.split()]

@expect_ok(arg_file('expr_parent.sh8'), LEX_PARSE)
def test_expr_parent(tree):
    assert tree == [WhenFunc(name='main', body=[
        WhenRet(expr=WhenBinOp(
            op='*',
            left=WhenBinOp(op='+', left=WhenInt(value=1),
                right=WhenInt(value=2)),
            right=WhenBinOp(op='-', left=WhenInt(value=4),
                right=WhenInt(value=2))
        ))
    ])]

@expect_ok(arg_file('vardecl_simple.sh8'), LEX_PARSE)
def test_vardecl_simple(tree):
    assert tree == [WhenFunc(name='main', body=[
        WhenVarDecl(type=byte, name='x', expr=WhenInt(value=0)),
        WhenRet(expr=WhenBinOp(op='+', left=WhenIdent(name='i'),
            right=WhenIdent(name='x'))),
    ])]

@expect_error(arg_file('vardecl_notype.sh8'), LEX_PARSE, ParserError)
def test_vardecl_notype(exn):
    pass

@expect_ok(arg_file('vardecl_expr.sh8'), LEX_PARSE)
def test_vardecl_expr(tree):
    assert tree == [WhenFunc(name='main', body=[
        WhenVarDecl(type=byte, name='x', expr=WhenBinOp(op='+',
            left=WhenBinOp(op='*', left=WhenInt(value=1),
                right=WhenIdent(name='i')),
            right=WhenInt(value=3))),
        WhenRet(expr=WhenBinOp(op='+', left=WhenIdent(name='i'),
            right=WhenIdent(name='x'))),
    ])]

@expect_ok(arg_file('call_noarg.sh8'), LEX_PARSE)
def test_call_noarg(tree):
    assert tree == [WhenFunc(name='main', body=[
        WhenExpr(expr=WhenCall(name='main', args=[])),
    ])]

@expect_ok(arg_file('call_onearg.sh8'), LEX_PARSE)
def test_call_onearg(tree):
    assert tree == [
        WhenFunc(name='neg', rettype=byte,
            args=[WhenFuncArg(type=byte, name='i')],
            body=[WhenRet(expr=WhenPreOp(op='-', expr=WhenIdent(name='i')))]),
        WhenFunc(name='main', rettype=void, args=[],
            body=[WhenVarDecl(type=byte, name='x', expr=WhenBinOp(op='+',
                left=WhenCall(name='neg', args=[WhenInt(value=1)]),
                right=WhenInt(value=1)))])
    ]

@expect_ok(arg_file('call_manyargs.sh8'), LEX_PARSE)
def test_call_manyargs(tree):
    assert tree == [
        WhenFunc(name='add', rettype=byte,
            args=[
                WhenFuncArg(type=byte, name='a'),
                WhenFuncArg(type=byte, name='b'),],
            body=[WhenRet(expr=WhenBinOp(op='+',
                left=WhenIdent(name='a'), right=WhenIdent(name='b')))]),
        WhenFunc(name='main', rettype=byte, args=[],
            body=[WhenRet(expr=WhenCall(name='add', args=[
                WhenInt(value=1), WhenInt(value=2)]))]),
    ]

@expect_ok(arg_file('affect_simple.sh8'), LEX_PARSE)
def test_affect_simple(tree):
    assert tree[:-1] == [
        WhenFunc(name='add', rettype=byte,
            args=[
                WhenFuncArg(type=byte, name='a'),
                WhenFuncArg(type=byte, name='b'),],
            body=[
                WhenAffect(left=WhenIdent(name='a'),
                    right=WhenBinOp(op='+',
                        left=WhenIdent(name='a'), right=WhenIdent(name='b'))),
                WhenRet(expr=WhenIdent(name='a'))])]

            #TODO: test negative litterals

@expect_ok(arg_file('affect_expr.sh8'), LEX_PARSE)
def test_affect_expr(tree):
    assert tree[:-1] == [
        WhenFunc(name='add', rettype=byte,
            args=[
                WhenFuncArg(type=byte, name='a'),
                WhenFuncArg(type=byte, name='b'),],
            body=[
                WhenAffect(
                    left=WhenBinOp(op='*',
                        left=WhenPreOp(op='-', expr=WhenIdent(name='b')),
                        right=WhenCall(name='get', args=[
                            WhenIdent(name='a'), WhenInt(value=1)])),
                    right=WhenBinOp(op='+',
                        left=WhenIdent(name='a'), right=WhenIdent(name='b'))),
                WhenRet(expr=WhenIdent(name='a'))])]

@expect_ok(arg_file('vardecl_sametype.sh8'), LEX_PARSE)
def test_vardecl_sametype(tree):
    pass

@expect_ok(arg_file('func_sametype.sh8'), LEX_PARSE)
def test_func_sametype(tree):
    pass

@expect_ok(arg_file('if_single_empty.sh8'), LEX_PARSE)
def test_if_empty(tree):
    assert tree == [WhenFunc(name='main', rettype=void, args=[], body=[
        WhenIf(alts=[
            WhenIfAlt(cond=WhenInt(value=1), body=[])
        ], default=[]),
    ])]

@expect_ok(arg_file('if_else_empty.sh8'), LEX_PARSE)
def test_if_else_empty(tree):
    assert tree == [WhenFunc(name='main', rettype=void, args=[], body=[
        WhenIf(alts=[
            WhenIfAlt(cond=WhenInt(value=1), body=[])
        ], default=[]),
    ])]

@expect_ok(arg_file('if_elif_else_simple.sh8'), LEX_PARSE)
def test_if_elif_else_empty(tree):
    x = WhenIdent(name='x')
    res = WhenIdent(name='res')
    assert tree == [WhenFunc(name='main', rettype=void, args=[], body=[
        WhenVarDecl(type=byte, name='x', expr=WhenInt(value=0)),
        WhenVarDecl(type=byte, name='res', expr=WhenInt(value=0)),
        WhenIf(
            alts=[
                WhenIfAlt(
                    cond=WhenBinOp(op='==',
                        left=x, right=WhenInt(value=1)),
                    body=[WhenAffect(left=res, right=WhenInt(value=10))]),
                WhenIfAlt(
                    cond=WhenBinOp(op='!=',
                        left=x, right=WhenInt(value=2)),
                    body=[WhenAffect(left=res, right=WhenInt(value=20))]),
            ],
            default=[WhenAffect(left=res, right=x)],
        ),
    ])]

@expect_ok(arg_file('while_empty.sh8'), LEX_PARSE)
def test_while_empty(tree):
    assert tree == [WhenFunc(name='main', rettype=void, args=[], body=[
        WhenWhile(cond=WhenInt(value=0), body=[]),
    ])]

@expect_ok(arg_file('while_cond_complex.sh8'), LEX_PARSE)
def test_while_cond_complex(tree):
    x = WhenIdent(name='x')
    assert tree == [WhenFunc(name='main', rettype=void, args=[], body=[
        WhenVarDecl(name='x', type=byte, expr=WhenInt(value=10)),
        WhenWhile(
            cond=o('or',
                o('and', o('!=', x, 2), o('==', o('&', x, 1), 0)),
                o('==', o('&', x, 1), 1)),
            body=[WhenAffect(left=x, right=o('-', x, 1))])
    ])]

@expect_ok(arg_file('do_while_empty.sh8'), LEX_PARSE)
def test_do_while_empty(tree):
    assert tree == [WhenFunc(name='main', rettype=void, args=[], body=[
        WhenDoWhile(cond=WhenInt(value=0), body=[])
    ])]

@expect_ok(arg_file('do_while_simple.sh8'), LEX_PARSE)
def test_do_while_simple(tree):
    two_power_7, n = WhenIdent(name='two_power_7'), WhenIdent(name='n')
    assert tree == [WhenFunc(name='main', rettype=void, args=[], body=[
        WhenVarDecl(type=byte, name='two_power_7', expr=WhenInt(value=1)),
        WhenVarDecl(type=byte, name='n', expr=WhenInt(value=0)),
        WhenDoWhile(cond=o('<', n, 7), body=[
            WhenAffect(left=two_power_7, right=o('+', two_power_7, 1)),
            WhenAffect(left=n, right=o('+', n, 1)),
        ])
    ])]


# TODO:
#   -   try to provoque more parsing errors
