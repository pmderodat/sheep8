#! /usr/bin/env python3
from setuptools import setup, find_packages

from sheep8 import VERSION

setup(
    name = 'Sheep-8',
    version = VERSION,
    packages = ['sheep8'],
    scripts = ['sheep8/sheep8'],
)
