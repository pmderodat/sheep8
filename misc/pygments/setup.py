from setuptools import setup

setup(
    name = 'pygments-sheep8',
    version = '0.1',
    py_modules = ['pygments_sheep8'],

    install_requires = ['pygments'],

    entry_points = {
        'pygments.lexers': 'sheep8lexer = pygments_sheep8:Sheep8Lexer',
    },

    author = 'Pierre-Marie de Rodat',
    author_email = 'pmderodat@kawie.fr',
    description = 'Pygments lexer for the Sheep8 language',
    license = 'GPL',
    keywords = 'pygments sheep8',
    # url = 'http://sheep8.epita.fr/',
)
