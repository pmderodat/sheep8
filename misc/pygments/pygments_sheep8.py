from pygments.lexer import RegexLexer, include, bygroups, using, this
from pygments.token import *

class Sheep8Lexer(RegexLexer):
    name = 'Sheep-8'
    aliases = ['sheep8', 'sh8']
    filenames = ['*.sh8', '*.sheep8']

    keywords = (
        'def', 'return', 'let', 'if', 'elif', 'else',
        'while', 'do', 'continue', 'break', 'or', 'and', 'not'
    )

    builtins = ()
    builtin_types = ('byte', 'void')

    operators = (
        r'<', r'<=', r'>', r'>=', r'==', r'!=',
        r'\|', r'&', r'\^', r'~', r'\+', r'-', r'\*',
        r'\+\+', r'--', r'<<', r'>>',
        r'\+=', r'-=', r'\*=', r'/=', r'%=', r'<<=', r'>>=', r'&=', r'\^=',
        r'\|=', r'[=]',
    )

    punctuation = (
        r'\(', r'\)', r',',
    )

    tokens = {
        'whitespace': [
            (r'#.*$', Comment),
            (r'\s+', Text),
        ],

        'types': [
            (r'(%s)\b' % '|'.join(builtin_types), Name.Builtin),
        ],

        'statement': [
            include('whitespace'),

            (r':', Punctuation), #, 'statement'),
            (r'end', Keyword, '#pop'),

            (r'(%s)\b' % '|'.join(keywords), Keyword),
            (r'(%s)' % '|'.join(operators), Operator),
            (r'(%s)' % '|'.join(punctuation), Punctuation),

            (r'[0-9]+', Number.Integer),
            (r'0b[01]*', Number.Oct),
            (r'0o[1-7]*', Number.Oct),
            (r'0x[0-9A-Fa-f]+', Number.Hex),

            (r'([a-zA-Z_][a-zA-Z0-9_]*)(\s*)(\()',
                bygroups(Name.Function, Text, Punctuation)),

            include('types'),
            (r'[a-zA-Z_][a-zA-Z0-9_]*', Name),
        ],

        'function': [
            include('whitespace'),
            include('types'),
            (r'[a-zA-Z_][a-zA-Z0-9_]*', Name),
            (r'[(),]', Punctuation),
            (r':', Punctuation, ('#pop', 'statement')),
        ],

        'root': [
            include('whitespace'),
            (r'def', Keyword),
            (r'[a-zA-Z_][a-zA-Z0-9_]*', Name.Function, 'function'),
        ],
    }
