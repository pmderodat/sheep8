from lexy import (
    BaseLexer,
    Integer, Identifier, Keyword, Symbol,
    integer, identifier, keyword, symbol,
)
from lexy.token import Token as BaseToken
from funcparserlib import parser as fpl

from sheep8 import ast
from sheep8.utils import or_all

inst = ast.instanciate


# Each class has a different associativity "priority". Topmost has the highest
# priority.
binop_classes = (
    ('bit_shift', {'>>', '<<'}, symbol),
    ('bit_and', {'&'}, symbol),
    ('bit_or', {'|', '^'}, symbol),
    ('multiplication', {'*'}, symbol),
    ('addition', {'+', '-'}, symbol),
    ('comparison', {'==', '!=', '<', '>', '<=', '>='}, symbol),
    ('logic_and', {'and'}, keyword),
    ('logic_or', {'or'}, keyword),
)

preops = {
    'logic_not': ('not', keyword),
    'bit_not': ('~', symbol),
    'opposite': ('-', symbol),
}

op_symbols = set()
op_keywords = set()
for _, tokens, token_type in binop_classes:
    if token_type == symbol:
        op_symbols |= tokens
    elif token_type == keyword:
        op_keywords |= tokens
for token, token_type in preops.values():
    if token_type == symbol:
        op_symbols.add(token)
    elif token_type == keyword:
        op_keywords.add(token)

class Lexer(BaseLexer):
    keywords = {
        'def', 'let', 'struct', 'if', 'else', 'elif', 'while', 'do', 'end',
        'return'} | op_keywords
    symbols = {':', '[', ']', '(', ')', ',', '='} | op_symbols
    line_comment_delimiter = '#'

def make_type(token):
    return inst(ast.Type, token,
        token, token.value)

type_ = fpl.a(identifier) >> make_type


def make_integer(token):
    return inst(ast.Int, token, token,
        token.value)

def make_ident(token):
    return inst(ast.Ident, token, token,
        token.value)

def make_call_args(tokens):
    if not tokens or not tokens[0]:
        # There is no argument
        return []
    elif not tokens[1]:
        # There is only one argument
        return [tokens[0]]
    else:
        return [tokens[0]] + list(tokens[1])

def make_call(tokens):
    return inst(ast.Call, tokens[0], tokens[-1],
        name=tokens[0].value, args=tokens[2])

expression = fpl.fwd()
expression.named('expression')

integer = fpl.a(integer) >> make_integer
integer.named('integer')
ident = fpl.memoize(fpl.a(identifier)) >> make_ident
ident.named('identifier')
call_args = fpl.maybe(
    expression + fpl.many(fpl.skip(fpl.a(Symbol(','))) + expression)
) >> make_call_args
call = (
    fpl.memoize(fpl.a(identifier)) +
    fpl.a(Symbol('(')) + call_args + fpl.a(Symbol(')'))
) >> make_call
call.named('call')
parents = (
    fpl.skip(fpl.a(Symbol('('))) + expression +
    fpl.skip(fpl.a(Symbol(')')))
)
parents.named('parented_expr')


simple_expr = parents | integer | call | ident

def make_preop(tokens):
    return inst(ast.PreOp, tokens[0], tokens[1],
        op=tokens[0].value, expr=tokens[1])

preop = (
    or_all(
        fpl.a(token_type.replace(value=token))
        for token, token_type in preops.values()
    ) + simple_expr) >> make_preop
preop.named('prefix_op')

def make_binop(tokens):
    left, remainder = tokens[0], tokens[1]
    if not remainder:
        # This is not really a binop, but just one expression
        return left

    op = remainder[0].value
    right = remainder[1]
    result = inst(ast.BinOp, left, right,
        op, left, right)
    return result

last_expr_class = simple_expr | preop
for class_name, tokens, token_type in binop_classes:
    expr_class = fpl.fwd()
    expr_class.named(class_name)
    result = expr_class >> make_binop
    expr_class.define(
        last_expr_class + fpl.maybe(
            or_all(
                fpl.a(token_type.replace(value=token))
                for token in tokens
            ) +
            result
        )
    )
    last_expr_class = result

expression.define(last_expr_class)


def make_stmt_expr(token):
    return inst(ast.Expr, token, token, expr=token)

def make_stmt_ret(tokens):
    return inst(ast.Ret, tokens[0], tokens[1],
        expr=tokens[1])

def make_stmt_vardecl(tokens):
    return inst(ast.VarDecl, tokens[0], tokens[1],
        type=tokens[0], name=tokens[1].value, expr=tokens[2])

def make_stmt_affect(tokens):
    return inst(ast.Affect, tokens[0], tokens[1],
        left=tokens[0], right=tokens[1])

stmt_block = fpl.fwd()
stmt_block.named('stmt_block')

stmt_expr = expression >> make_stmt_expr
stmt_expr.named('stmt_expr')
stmt_ret = (fpl.a(Keyword('return')) + expression) >> make_stmt_ret
stmt_ret.named('ret')
stmt_vardecl = (
    fpl.skip(fpl.a(Keyword('let'))) + type_ + fpl.a(identifier) +
    fpl.skip(fpl.a(Symbol('='))) + expression
) >> make_stmt_vardecl
stmt_vardecl.named('variable_decl')

stmt_affect = (
    fpl.memoize(expression) + fpl.skip(fpl.a(Symbol('='))) + expression
) >> make_stmt_affect
stmt_affect.named('affectation')

def make_if_alt(tokens):
    body = tokens[-1]
    if not body:
        last_token = tokens[-2]
    else:
        last_token = body[-1]
    return inst(ast.IfAlt, tokens[0], last_token,
        cond=tokens[1], body=body or [])
def make_if(tokens):
    return inst(ast.If, tokens[0], tokens[-1],
        alts=[tokens[0]] + tokens[1], default=tokens[-2] or [])

def make_stmt_while(tokens):
    return inst(ast.While, tokens[0], tokens[-1],
        cond=tokens[1], body=tokens[2])
def make_stmt_do_while(tokens):
    return inst(ast.DoWhile, tokens[0], tokens[-1],
        cond=tokens[2], body=tokens[1])

stmt_if = (
    ((fpl.a(Keyword('if')) + expression + fpl.skip(fpl.a(Symbol(':'))) +
        stmt_block) >> make_if_alt) +
    fpl.many((
        fpl.a(Keyword('elif')) + expression + fpl.skip(fpl.a(Symbol(':'))) +
        stmt_block) >> make_if_alt) +
    fpl.maybe(
        fpl.skip(fpl.a(Keyword('else')) + fpl.a(Symbol(':'))) +
        stmt_block) +
    fpl.a(Keyword('end'))) >> make_if
stmt_if.named('if')

stmt_while = (
    fpl.a(Keyword('while')) + expression + fpl.skip(fpl.a(Symbol(':'))) +
    stmt_block + fpl.a(Keyword('end'))) >> make_stmt_while
stmt_while.named('while')

stmt_do_while = (
    fpl.a(Keyword('do')) + fpl.skip(fpl.a(Symbol(':'))) +
    stmt_block +
    fpl.skip(fpl.a(Keyword('end')) + fpl.a(Keyword('while'))) +
    expression) >> make_stmt_do_while
stmt_do_while.named('do_while')

statement = (
    stmt_ret | stmt_vardecl | stmt_affect |
    stmt_if | stmt_while | stmt_do_while | stmt_expr)
stmt_block.define(fpl.many(statement))
statement.named('statement')


def make_funcarg(tokens):
    type_, tok_ident = tokens
    return inst(ast.FuncArg, type_, tok_ident,
        type_, tok_ident.value)

def make_funcargs(tokens):
    if not tokens or not tokens[0]:
        # There is no argument
        return []
    elif not tokens[1]:
        # There is only one argument
        return [tokens[0]]
    else:
        return [tokens[0]] + list(tokens[1])

function_arg = (type_ + fpl.a(identifier)) >> make_funcarg
function_args = fpl.maybe(
    function_arg + fpl.many(fpl.skip(fpl.a(Symbol(','))) + function_arg)
) >> make_funcargs


def make_func(tokens):
    return inst(ast.Func, tokens[1], tokens[-1],
        name=tokens[1].value, args=tokens[3],
        rettype=tokens[5], body=tokens[7])

function = (
    fpl.a(Keyword('def')) + fpl.a(identifier) +
    fpl.a(Symbol('(')) + function_args + fpl.a(Symbol(')')) +
    type_ + fpl.a(Symbol(':')) +
    stmt_block + fpl.a(Keyword('end'))
) >> make_func
function.named('function')


parser = fpl.many(function) + fpl.skip(fpl.eof)
parser.named('global')

def lex_and_parse(fp):

    # TODO: fix funcparserlib to make it accept "location" as a token position.
    class Token(BaseToken):
        __slots__ = ('type', 'value', 'location', 'pos')
        def __init__(self, *args, **kwargs):
            super(Token, self).__init__(*args, **kwargs)
            self.pos = self.location

    return parser.parse(
        [Token(tok.type, tok.value, tok.location) for tok in Lexer(fp)])
