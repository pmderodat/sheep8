from sheep8.ast import WhenType
from sheep8.environ import Environ
from sheep8.errors import CompilationError, IncompatibleTypeError


class BuiltinType:

    def __init__(self, name, size):
        self.name = name
        self.size = size

    def __str__(self):
        return self.name

void = BuiltinType('void', size=0)
byte = BuiltinType('byte', size=1)


class TypeEnviron(Environ):

    def __init__(self, parent=None, func_sym=None):
        '''
        A type environ that is attached to a function must know it. This
        enables the type environ to provide the function return type.
        '''
        super(TypeEnviron, self).__init__(parent)
        self.func_sym = func_sym

    def get_function(self):
        if self.func_sym:
            return self.func_sym
        elif self.parent:
            return self.parent.get_function()
        else:
            assert False, 'Tried to get return type out of any function'

    def canonize(self, tree):
        '''
        Convert an AST-type node to a proper type value (a singleton). Raise an
        UnknownTypeError if some typename does not exist.
        '''
        if tree != WhenType():
            assert False, 'I was asked to convert a non-type AST node'

        typename = tree.name
        try:
            return self.lookup(typename)
        except KeyError:
            raise CompilationError(
                'Unknown typename: {}'.format(typename), tree.location)


# Types compatibility routines

def can_assign(place, value):
    '''
    Return if `value` can be assigned to `place`. Both of these arguments must
    be types.
    '''

    if place is void:
        # No variable can be void, but the result of a procedure call can be
        # passed to return.
        return value is void
    if place == byte:
        # There is only one kind of integer right now: byte.
        return value is byte

    # Yes, this could be summarized to `place is value`.

    return False


_bin_arith_msg = 'Integer operations can only work with bytes'
def _is_compatible_bin_ops(left, right):
    if left is not byte:
        raise IncompatibleTypeError(_bin_arith_msg, left, 'left operand')
    if right is not byte:
        raise IncompatibleTypeError(_bin_arith_msg, right, 'right operand')
    return byte

def _is_compatible_neg(expr):
    if expr is not byte:
        raise IncompatibleTypeError(
            'Only bytes have an opposite', expr, 'operand')
    return byte

_binary_ops = {
    op: _is_compatible_bin_ops
    for op in '>> << & | * + - == != < > <= >= and or'.split()
    # All binary operators have the same compatibility rule: accept only bytes
}

_prefix_ops = {
    op: _is_compatible_neg
    for op in '- not ~'.split()
    # All prefix operators have the same compatibility rule: accept only bytes
}

def get_binop_type(op, left, right):
    try:
        getter = _binary_ops[op]
    except KeyError:
        assert False, 'Invalid binary operator: {}'.format(op)
    return getter(left, right)

def get_preop_type(op, expr):
    try:
        getter = _prefix_ops[op]
    except KeyError:
        assert False, 'Invalid prefix operator: {}'.format(op)
    return getter(expr)
