#! /usr/bin/env python3

import os.path
import sys

from funcparserlib.parser import ParserError
from lexy import LexingError

from sheep8 import ast
from sheep8.canonical import Canonicaliser
from sheep8.chip8 import WhenProcFrag, Chip8Frame
from sheep8.errors import CompilationError
from sheep8.parser import lex_and_parse
from sheep8.semantic import Semantic
from sheep8.utils import (
    format_location, pretty_print, pretty_print_linear_ir)


prog_frag = WhenProcFrag()


def handle_error(program, e):
    message = e.args[0]
    if len(e.args) >= 2 and e.args[1]:
        message = '{}: {}: {}'.format(
            program, format_location(e.args[1], True), message)
    print(message, file=sys.stderr)
    sys.exit(1)


def main(argv):
    program = os.path.basename(argv[0])
    with open(argv[1], 'r') as source:
        try:
            tree = lex_and_parse(source)
        except (LexingError, ParserError) as e:
            handle_error(program, e)

    semantic = Semantic(Chip8Frame)
    try:
        cmpl_prg = semantic.process(tree)
    except CompilationError as e:
        handle_error(program, e)

    canonicaliser = Canonicaliser(cmpl_prg)
    cmpl_prg.map_proc(canonicaliser.canonicalise)

    for frag in cmpl_prg.fragments:
        if frag == prog_frag:
            print('-----------')
            print('{}:'.format(frag.frame.label_start.name))
            print('-----------')
            pretty_print_linear_ir(frag.body)
        else:
            assert False, 'Invalid fragment: {}'.format(frag)
