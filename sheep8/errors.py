class CompilationError(Exception):
    '''
    This is the only error thas is allowed to pass main entry points.
    Internal errors must be converted to this if they are fatal.
    '''
    def __init__(self, message, location):
        super(CompilationError, self).__init__(message, location)

class InternalError(Exception):
    '''
    This kind of error must not pass main entry points. To to do, convert it to
    CompilationError using the `to_external` method.
    '''
    def get_message(self, *args, **kwargs):
        '''
        Subclasses must override this method to provide a suitable error
        message for conversion to CompilationError.
        '''
        raise NotImplementedError()

    def to_external(self, location, *args, **kwargs):
        return CompilationError(
            self.get_message(*args, **kwargs),
            location
        )

class IncompatibleTypeError(InternalError):
    def __init__(self, message, type, position):
        super(IncompatibleTypeError, self).__init__()
        self.message = message
        self.type = type
        self.position = position

    def get_message(self):
        return '{} ({} is {})'.format(
            self.message, self.position, self.type)
