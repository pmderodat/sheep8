from sheep8.utils import record


# Intermediate code expressions
Const, WhenConst    = record('IRConst', 'value')
Name,  WhenName     = record('IRName',  'label')
Temp,  WhenTemp     = record('IRTemp',  'temp')
BinOp, WhenBinOp    = record('IRBinOp', 'op left right')
Mem,   WhenMem      = record('IRMem',   'expr')
Call,  WhenCall     = record('IRCall',  'label args')
ESeq,  WhenESeq     = record('IRESeq',  'stmt expr')

# Intermediate code statements
Move,  WhenMove     = record('IRMove',  'dst src')
Expr,  WhenExpr     = record('IRExpr',  'expr')
Jump,  WhenJump     = record('IRJump',  'dst labels')
CJump, WhenCJump    = record('IRCJump', 'op left right lbtrue lbfalse')
Seq,   WhenSeq      = record('IRSeq',   'stmt1 stmt2')
Label, WhenLabel    = record('IRLabel', 'label')

# Available binary operations (for BinOp)
PLUS, MINUS, MUL, DIV, AND, OR, LSHIFT, RSHIFT, ARSHIFT, XOR = range(10)

# Available relational operations (for CJump)
EQ, NE, LT, GT, LE, GE, ULT, ULE, UGT, UGE = range(10)
# Negation table for relational operations
REL_NEG = {
    EQ: NE,
    NE: EQ,

    LT: GE,
    GT: LE,
    LE: GT,
    GE: LT,

    ULT: UGE,
    UGT: ULE,
    ULE: UGT,
    UGE: ULT,
}

# Expressions categories
Ex, WhenEx = record('Ex', 'expr') # This is a mere expression
# This is a conditional expression: must return a boolean when used in
# expressions, and used to jump in conditional
# generator must be a callback:
#   program.Label * program.Label -> IR statement
Cx, WhenCx = record('Cx', 'generator')

is_ex = WhenEx()
is_cx = WhenCx()


class Translator:
    '''
    Gather a program alongside with methods that translate AST nodes to IR
    nodes. Each method takes a specific kind of AST node and return a IR
    translation.

    If these methods receive bad input, they assert False since the semantic
    pass is supposed to filter bad input out.
    '''

    translate_binop = {
        # AST op -> (IR op, is_boolean)
        '+':    (PLUS,      False),
        '-':    (MINUS,     False),
        '*':    (MUL,       False),
        '<<':   (LSHIFT,    False),
        '>>':   (RSHIFT,    False),
        '^':    (XOR,       False),
        '&':    (AND,       False),
        '|':    (OR,        False),
        '==':   (EQ,        True),
        '!=':   (NE,        True),
        '<':    (LT,        True),
        '>':    (GT,        True),
        '<=':   (LE,        True),
        '>=':   (GE,        True),
    }
    def __init__(self, program):
        self.program = program

    # Conversion from categorized expressions

    def convert_to_value(self, expr):
        '''
        Return an IR expression that evaluates the given `expr`.
        '''
        if expr == is_ex:
            return expr.expr
        elif expr == is_cx:
            res = Temp(self.program.create_temp())
            t = self.program.create_label()
            f = self.program.create_label()
            return ESeq(
                self.trans_block([
                    Move(res, Const(1)),
                    expr.generator(t, f),
                    Label(f),
                    Move(res, Const(0)),
                    Label(t),
                ]), res)
        else:
            assert False, (
                'Invalid kind of expression category: {}'.format(expr))

    def convert_to_condition(self, expr):
        '''
        Return a IR node generator (program.Label * program.Label) -> IR expr
        The returned IR expr jumps to the first label when the conditional
        evaluates to true and jumps to false otherwise.
        '''
        if expr == is_ex:
            return (lambda t, f: CJump(NE, expr.expr, Const(0), t, f))
        elif expr == is_cx:
            return expr.generator
        else:
            assert False, (
                'Invalid kind of expression category: {}'.format(expr))


    # Translation of expressions
    # They all return a categorized expression.

    def trans_ident(self, sym, frame):
        return Ex(frame.get_access(sym.storage))

    def trans_byte(self, byte_expr):
        return Ex(Const(byte_expr.value))

    def trans_preop(self, op, sub_expr):
        if op == '-':
            expr = self.convert_to_value(sub_expr)
            return Ex(BinOp(MINUS, Const(0), expr))
        elif op == 'not':
            return Cx(lambda t, f: self.convert_to_condition(sub_expr)(f, t))
        elif op == '~':
            expr = self.convert_to_value(sub_expr)
            return Ex(BinOp(XOR, Const(0xff), expr))
        else:
            assert False, 'Invalid prefix operator: {}'.format(preop)

    def trans_binop(self, op, left, right):
        # Logic `and` and `or` are special cases
        if op in ('and', 'or'):
            def generate(t, f):
                inter_lbl = self.program.create_label()
                # Only first CJump labels change between 'and' and 'or'
                first_t, first_f = (
                    (inter_lbl, f)
                    if op == 'and' else
                    (t, inter_lbl))

                return self.trans_block([
                    self.convert_to_condition(left)(first_t, first_f),
                    Label(inter_lbl),
                    self.convert_to_condition(right)(t, f)
                ])
            return Cx(generate)

        try:
            ir_op, is_bool = self.translate_binop[op]
        except KeyError:
            assert False, 'Invalid binary operator: {}'.format(op)

        left = self.convert_to_value(left)
        right = self.convert_to_value(right)
        if is_bool:
            return Cx(lambda t, f: CJump(ir_op, left, right, t, f))
        else:
            return Ex(BinOp(ir_op, left, right))

    def trans_call(self, sym, args):
        return Ex(Call(
            sym.label, [self.convert_to_value(arg) for arg in args]))


    # Translation of statements
    def trans_vardecl(self, sym, expr, frame):
        return self.trans_affect(
            self.trans_ident(sym, frame), expr)

    def trans_affect(self, lexpr, rexpr):
        return Move(
            self.convert_to_value(lexpr), self.convert_to_value(rexpr))

    def trans_if(self, alts, default):
        end_lbl = self.program.create_label()
        stmts = []
        for alt_cond, alt_body in alts:
            # Here we start the code handling current branch's condition. If
            # the condition of this branch is true, go to the actual branch
            # instructions (pointed by current_branch_label), otherwise, go to
            # the next branch's condition.
            current_branch_label = self.program.create_label()
            next_condition_label = self.program.create_label()
            stmts.append(self.convert_to_condition(alt_cond)(
                current_branch_label, next_condition_label))

            # Here is the actual branch instructions
            stmts.append(Label(current_branch_label))
            stmts.append(alt_body)
            # Do not forget to jump at the end of the if block!
            stmts.append(Jump(Name(end_lbl), [end_lbl]))

            # And then we put the next condition label (which is instead
            # The 'else' branch if we are in the last conditional (if/elif)
            # branch.
            stmts.append(Label(next_condition_label))
        stmts.append(default)
        stmts.append(Jump(Name(end_lbl), [end_lbl]))
        stmts.append(Label(end_lbl))
        return self.trans_block(stmts)

    def trans_while(self, cond, body, exec_once):
        stmts = []
        end_lbl = self.program.create_label()
        body_lbl = self.program.create_label()
        # If the first iteration is not required, test for the condition at the
        # beginning.
        if not exec_once:
            stmts.append(self.convert_to_condition(cond)(
                body_lbl, end_lbl))
        stmts.append(Label(body_lbl))
        stmts.append(body)
        stmts.append(self.convert_to_condition(cond)(
            body_lbl, end_lbl))
        stmts.append(Label(end_lbl))
        return self.trans_block(stmts)

    def trans_return(self, expr, frame):
        return Seq(
            Move(frame.get_return_val(), self.convert_to_value(expr)),
            Jump(Name(frame.label_end), [frame.label_end])
        )

    def trans_expr(self, expr):
        return Expr(self.convert_to_value(expr))

    def trans_block(self, stmts):
        '''
        Translate a list of IR statement nodes to a single IR statement node.
        '''
        if stmts:
            itr = iter(stmts)
            ir_tree = next(itr)
            for stmt in itr:
                ir_tree = Seq(ir_tree, stmt)
            return ir_tree
        else:
            return Expr(Const(0)) # This is a NOP

    def trans_func(self, stmts, frame):
        return Seq(Label(frame.label_start), stmts)
