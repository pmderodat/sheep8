from sheep8.translate import (
    WhenConst, WhenName, WhenTemp, WhenBinOp, WhenMem, WhenCall, WhenESeq,
    WhenMove, WhenExpr, WhenJump, WhenCJump, WhenSeq, WhenLabel,
    Name, Temp, BinOp, Mem, Call,
    Move, Expr, Jump, CJump, Label,
    REL_NEG)
from sheep8.utils import record

WConst = WhenConst()
WName = WhenName()
WTemp = WhenTemp()
WBinOp = WhenBinOp()
WMem = WhenMem()
WCall = WhenCall()
WESeq = WhenESeq()

WMove = WhenMove()
WExpr = WhenExpr()
WJump = WhenJump()
WCJump = WhenCJump()
WSeq = WhenSeq()
WLabel = WhenLabel()

stmt_expr_const = WhenExpr(expr=WConst)


BasicBlock, WhenBasicBlock = record('BasicBlock', 'name stmts successors')


class Canonicaliser:
    '''
    Turn a raw IR tree into a canonical one.
    '''

    def __init__(self, program):
        self.program = program


    def can_commute(self, stmts, expr):
        '''
        Return if a block of statement and an expression can commute their
        execution order.
        '''
        # TODO: try to detect more commutability cases
        if all(stmt == stmt_expr_const for stmt in stmts):
            return True
        elif expr in (WName, WConst):
            return True
        else:
            return False

    def _linearise_expr(self, expr):
        '''
        Turn a raw IR expression and turn it to a linearised one. Return a
        list of statements and an expression. The list of statements must be
        executed before the returned expression.
        '''
        if expr in (WConst, WName, WTemp):
            # A {Const, Name, Temp} is already a linearised one.
            return [], expr
        elif expr == WBinOp:
            pre_stmts_left, left = self._linearise_expr(expr.left)
            pre_stmts_right, right = self._linearise_expr(expr.right)
            # If left and pre_stmts_right do not commute, compute left right
            # after pre_stmts_left and store it for the binary operation.
            if not self.can_commute(pre_stmts_right, left):
                left_temp = self.program.create_temp()
                pre_stmts_left.expand(
                    Move(left_temp, left))
                left = Temp(left)
            return (
                pre_stmts_left + pre_stmts_right,
                BinOp(expr.op, left, right))
        elif expr == WMem:
            pre_stmts, expr = self._linearise_expr(expr.expr)
            return (pre_stmts, Mem(expr))
        elif expr == WCall:
            pre_stmts_list = [] # List of side effects for each argument
            pre_stmts = []      # Final list of side effects
            args = []           # List of arguments before commutation
            final_args = []     # List of arguments after commutation
            # First compute side effects and linearised expression for each
            # argument.
            for arg in expr.args:
                sub_pre_stmts, linear_arg = self._linearise_expr(arg)
                pre_stmts_list.extend(sub_pre_stmts)
                args.append(linear_arg)
            # Then for each argument, if next arguments' side effects collide
            # with the argument value computing, evaluate it first. In other
            # words: if argument 3 value computing can be affected by arguments
            # 4/5/... side effects, compute argument 3 before executing
            # arguments 4/5/... side effects.
            for i, pre_stmts_block, arg in enumerate(
                zip(pre_stmts_list, args)
            ):
                pre_stmts.extend(pre_stmts_block)
                if any(
                    not self.can_commute(pre_block, arg)
                    for pre_block in pre_stmts_block[i + 1:]
                ):
                    temp = Temp(self.program.create_temp())
                    pre_stmts.append(Move(temp, arg))
                    final_args.append(temp)
                else:
                    final_args.append(arg)
            # The whole colling process is: make the call as a previous
            # statement then the result expression is the stored return value.
            ret_value = Temp(self.program.create_temp())
            pre_stmts.append(Move(ret_value, Call(expr.label, final_args)))
            return (pre_stmts, ret_value)
        elif expr == WESeq:
            pre_stmts = self._linearise_stmt(expr.stmt)
            sub_pre_stmts, expr = self._linearise_expr(expr.expr)
            pre_stmts.extend(sub_pre_stmts)
            return pre_stmts, expr
        else:
            assert False, 'Invalid kind of IR expression: {}'.format(expr)

    def _linearise_stmt(self, stmt):
        '''
        Turn a raw IR statement to a linearised one and return it.
        '''
        result = []
        # Every time multiple expressions are computed in the save expression
        # (namely in Move and CJump nodes), the order of evaluation is
        # implementation-defined. It is up to the front-end to ensure that
        # there is no ambiguity.
        if stmt == WMove:
            pre_stmts_dst, dst = self._linearise_expr(stmt.dst)
            pre_stmts_src, src = self._linearise_expr(stmt.src)
            result.extend(pre_stmts_dst)
            result.extend(pre_stmts_src)
            result.append(Move(dst, src))
        elif stmt == WExpr:
            pre_stmts, expr = self._linearise_expr(stmt.expr)
            result.extend(pre_stmts)
            result.append(Expr(expr))
        elif stmt == WJump:
            pre_stmts, dst = self._linearise_expr(stmt.dst)
            result.extend(pre_stmts)
            result.append(Jump(dst, stmt.labels))
        elif stmt == WCJump:
            pre_stmts_left, left = self._linearise_expr(stmt.left)
            pre_stmts_right, right = self._linearise_expr(stmt.right)
            result.extend(pre_stmts_left)
            result.extend(pre_stmts_right)
            result.append(CJump(stmt.op, left, right, stmt.lbtrue,
                stmt.lbfalse))
        elif stmt == WSeq:
            result.extend(self._linearise_stmt(stmt.stmt1))
            result.extend(self._linearise_stmt(stmt.stmt2))
        elif stmt == WLabel:
            # A label is already a linearised label.
            return [stmt]
        else:
            assert False, 'Invalid kind of IR statement: {}'.format(stmt)
        return result

    def linearise(self, ir):
        '''
        Remove ESeq nodes from a IR tree and move Call nodes to top level.
        '''
        return self._linearise_stmt(ir)

    def _close_basic_block(self, bblock, successors, bblocks):
        # Every basic block *has* to start with a label
        bblock_name = bblock[0].label.name
        bblocks[bblock_name] = BasicBlock(
            bblock_name, bblock, successors)

    def split_basic_blocks(self, linear_ir, frame):
        '''
        Groups statements into code sequences with no internal jump nor label.
        '''
        bblocks = {}
        current_bblock = []
        for i, stmt in enumerate(linear_ir):
            if stmt == WLabel:
                if current_bblock:
                    # If there is a current basic block, close it with a jump
                    # to this label...
                    label = stmt.label
                    current_bblock.append(
                        Jump(Name(label), [label]))
                    self._close_basic_block(
                        current_bblock, [label.name], bblocks)
                # A label starts a new basic block.
                current_bblock = [stmt]
            elif stmt in (WJump, WCJump):
                # Close the current basic block with this jump.
                try:
                    next_stmt = linear_ir[i + 1]
                except IndexError:
                    pass
                else:
                    if next_stmt != WLabel:
                        # The next basic block seems to be unreachable code.
                        assert False, 'Unreachable code found'
                current_bblock.append(stmt)
                bblock_name = current_bblock[0].label.name
                successors = (
                    [label.name for label in stmt.labels]
                    if stmt == WJump else
                    [stmt.lbtrue.name, stmt.lbfalse.name])
                self._close_basic_block(
                    current_bblock, successors, bblocks)
                current_bblock = []
            elif stmt in (WMove, WExpr, WSeq):
                current_bblock.append(stmt)
            else:
                assert False, 'Invalid kind of IR statement: {}'.format(stmt)
        # If the current basic block is not closed yet, close it with a jump to
        # the frame's end label.
        if current_bblock:
            current_bblock.append(
                Jump(Name(frame.label_end), [frame.label_end]))
            self._close_basic_block(
                current_bblock, [frame.label_end.name], bblocks)
        return bblocks

    def schedule(self, basic_blocks, frame):
        '''
        Order basic blocks to minimize jumps and to place false branches of
        conditional jumps right after the branch jump.
        '''
        result = []
        # Start with the "entry point" basic block.
        first_bblock = frame.label_start.name
        remaining_bblocks = set(basic_blocks) - {first_bblock}
        # We consider that the end-of-function pseudo basic block is already
        # visited not to try to include it in the trace.
        visited_bblocks = {frame.label_end.name}
        while remaining_bblocks or first_bblock:
            if first_bblock:
                b = first_bblock
                first_bblock = None
            else:
                # Start a new trace: pick a random basic block...
                b = remaining_bblocks.pop()
            # ... then follow its successors
            while b not in visited_bblocks:
                visited_bblocks.add(b)
                basic_block = basic_blocks[b]
                result.extend(basic_blocks[b].stmts)
                if len(basic_block.successors) == 1:
                    # The last instruction is an unconditional jump: take the
                    # successor if it is not already visited.
                    if not (basic_block.successors[0] in visited_bblocks):
                        # Remove the ending jump instruction
                        result.pop()
                        b = basic_block.successors[0]
                    # Otherwise this is the end of the current trace.
                elif len(basic_block.successors) == 2:
                    # The last instruction is a conditional jump:
                    #   - if the false branch is not already visited take the
                    #     false branch (rightmost successor)
                    #   - if the true branch is not already visited, reverse
                    #     the condition and take the true branch (leftmost
                    #     successor)
                    #   - if both branches are visited, add a jump to the false
                    #     branch and close the current trace.
                    true_succ = basic_block.successors[0]
                    false_succ = basic_block.successors[1]
                    cjump = basic_block.stmts[-1]
                    if not (false_succ in visited_bblocks):
                        b = false_succ
                    elif not (true_succ in visited_bblocks):
                        cjump.op = REL_NEG[cjump.op]
                        b = true_succ
                    else:
                        lbl = self.program.create_label()
                        false_lbl = cjump.lbfalse
                        cjump.lbfalse = lbl
                        basic_block.stmts.extend((
                            Label(lbl),
                            Jump(Name(false_lbl))
                        ))
                        # Keep b the same to close the current truce.
                else:
                    assert False, (
                        'Invalid basic block (too many successors): {}'.format(
                            successors))
        return result

    def remove_unused_labels(self, ir):
        defined = set()
        used = set()
        for stmt in ir:
            if stmt == WLabel:
                defined.add(stmt.label.name)
            elif stmt == WJump:
                used.update(label.name for label in stmt.labels)
            elif stmt == WCJump:
                used.update((stmt.lbtrue.name, stmt.lbfalse.name))
        unused = defined - used
        return [
            stmt for stmt in ir
            if stmt != WLabel or stmt.label.name not in unused ]


    def canonicalise(self, ir, frame):
        linear_ir = self.linearise(ir)
        basic_blocks = self.split_basic_blocks(linear_ir, frame)
        scheduled_ir = self.schedule(basic_blocks, frame)
        return self.remove_unused_labels(scheduled_ir)
