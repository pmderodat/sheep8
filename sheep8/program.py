from sheep8.utils import record


Label, WhenLabel = record('Label', 'name')
Temp, WhenTemp = record('Temporary', 'num')

class Program:

    def __init__(self, frame_type):
        self.frame_type = frame_type
        self.frame_count = 0
        self.temp_count = 0
        self.label_count = 0
        self.fragments = []

    def add_fragment(self, frag):
        self.fragments.append(frag)

    def create_frame(self, name):
        self.frame_count += 1
        f = self.frame_type(self.frame_count, name)
        f.create_temp = self.create_temp
        f.create_label = self.create_label
        return f

    def create_temp(self):
        self.temp_count += 1
        return Temp(self.temp_count)

    def create_label(self):
        self.label_count += 1
        return Label('L{}'.format(self.label_count))

    def map_proc(self, func):
        for frag in self.fragments:
            self.frame_type.map_proc(frag, func)
