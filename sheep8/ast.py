from lexy.token import Location

from sheep8.utils import record


def instanciate(record_type, start, end, *args, **kwargs):
    '''
    Instanciate a record taking care of its location.
    '''
    args = (Location(start.location.start, end.location.end), ) + args
    return record_type(*args, **kwargs)


# Common
Type, WhenType = record('Type', 'location name')

# Expressions
Ident, WhenIdent = record('Identifier', 'location name')
Int, WhenInt = record('Integer', 'location value')
PreOp, WhenPreOp = record('PrefixOperator', 'location op expr')
BinOp, WhenBinOp = record('BinaryOperator', 'location op left right')
Call, WhenCall = record('Call', 'location name args')

# Statements
VarDecl, WhenVarDecl = record('VariableDeclaration', 'location type name expr')
Affect, WhenAffect = record('Affectation', 'location left right')
If, WhenIf = record('If', 'location alts default')
IfAlt, WhenIfAlt = record('IfAlternative', 'location cond body')
While, WhenWhile = record('While', 'location cond body')
DoWhile, WhenDoWhile = record('DoWhile', 'location cond body')
Ret, WhenRet = record('Return', 'location expr')
Expr, WhenExpr = record('Expression', 'location expr')

# Global declarations
FuncArg, WhenFuncArg = record('FunctionArgument', 'location type name')
Func, WhenFunc = record('Function', 'location name args rettype body')
