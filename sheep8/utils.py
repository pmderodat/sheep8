def format_location(location, capitalize=False):
    result = 'line {}, column {}'.format(*location.start)
    if capitalize:
        result = result.capitalize()
    return result

def align(value, unit):
    '''
    Align some integer according to the given unit size.
    '''
    return ((value + unit - 1) // unit) * unit

class _Any:
    '''
    Special value used to match any record field value.
    '''
    pass

def record(typename, field_names):
    '''
    Return a mutable named tuple class and a constructor to fill fields to
    "ANY" default value.
    '''

    # Accept field_names as a list of anything or a blank-separated names in a
    # string.
    if isinstance(field_names, str):
        field_names = field_names.replace(',', ' ').split()
    field_names = [str(name) for name in field_names]

    def cls_repr(self):
        return '{}({})'.format(typename, ', '.join(
            '{}={}'.format(name, repr(getattr(self, name)))
            for name in field_names
        ))

    def cls_eq(self, record):
        if type(record) != type(self):
            return False
        for name in field_names:
            field = getattr(self, name)
            other_field = getattr(record, name)
            if (not (field is _Any) and not (other_field is _Any)
                and other_field != field
            ):
                return False
        return True

    def cls_init(self, *args, **kwargs):
        for name in field_names:
            setattr(self, name, None)
        for name, arg in zip(field_names, args):
            setattr(self, name, arg)
        for name, value in kwargs.items():
            if name not in field_names:
                raise KeyError('No such field: {}'.format(name))
            setattr(self, name, value)
        super(result, self).__init__()

    result = type(
        typename, (object, ),
        {
            '__slots__': tuple(field_names),
            '_fields': field_names,
            '_name': typename,

            '__init__': cls_init,
            '__repr__': cls_repr,
            '__str__': cls_repr,
            '__eq__': cls_eq,
        })

    def when_ctor(**kwargs):
        fields = {name: _Any for name in field_names}
        fields.update(kwargs)
        return result(**fields)
    when_ctor.__name__= 'When{}'.format(typename)

    return result, when_ctor


def or_all(iterable):
    '''
    Return the disjunction ("|") of all items in `iterable`.
    '''
    itr = iter(iterable)
    try:
        result = next(itr)
    except StopIteration:
        raise ValueError('At least one item is needed')
    for item in itr:
        result |= item
    return result


def pretty_print(tree, indent='  ', print_types=False):
    '''
    Print an indented tree of records.
    '''
    if isinstance(tree, list):
        print('[')
        for node in tree:
            print(indent, end='')
            pretty_print(node, indent + '  ', print_types)
        print('{}]'.format(indent))
    elif isinstance(tree, dict):
        print('{')
        for key, value in tree.items():
            print('{}{}:'.format(indent, key), end='')
            pretty_print(value, indent + '    ', print_types)
        print('}')
    elif not hasattr(tree, '_name'):
        if print_types:
            print('{} ({})'.format(tree, type(tree)))
        else:
            print(tree)
    else:
        print('{}: '.format(tree._name))
        for field in tree._fields:
            if field == 'location':
                continue
            print('{}- {}: '.format(indent, field), end='')
            pretty_print(getattr(tree, field), indent + '  ', print_types)

def pretty_print_linear_ir(stmts):
    '''
    Print a linearised IR code like assembler code.
    '''
    from sheep8 import program
    from sheep8.translate import (
        PLUS, MINUS, MUL, DIV, AND, OR, LSHIFT, RSHIFT, ARSHIFT, XOR,
        EQ, NE, LT, GT, LE, GE, ULT, UGT, ULE, UGE,
        WhenConst, WhenName, WhenTemp, WhenBinOp, WhenMem, WhenCall,
        WhenMove, WhenExpr, WhenJump, WhenCJump, WhenLabel)

    is_temporary = program.WhenTemp()

    binops = {
        PLUS:     '+',      MINUS:  '-',
        MUL:      '*',      DIV:    '/',
        AND:      '&&',     OR:     '||',
        LSHIFT:   '+>>',    RSHIFT: '<<',
        ARSHIFT:  '>>',
        XOR:      '^',
    }
    relops = {
        EQ:   '=',      NE:     '!=',
        LT:   '<',      GT:     '>',
        LE:   '<=',     GE:     '>=',
        ULT:   '<',     UGT:    '>',
        ULE:   '<=',    UGE:    '>=',
    }
    expr_kinds = (
        # (expression match, repr)
        (WhenConst(), lambda const: str(const.value)),
        (WhenName(),  lambda name: '@{}'.format(name.label.name)),
        (WhenTemp(),  lambda temp: '%{}'.format(
            temp.temp.num   # This is a temporary
            if temp.temp == is_temporary else
            temp.temp.name  # This is a specific register
        )),
        (WhenBinOp(), lambda binop: '({} {} {})'.format(
            repr_expr(binop.left), binops[binop.op], repr_expr(binop.right))),
        (WhenMem(),   lambda mem: '[{}]'.format(
            repr_expr(mem.expr))),
        (WhenCall(),  lambda call: '{}({})'.format(call.label.name,
            ', '.join(repr_expr(arg) for arg in call.args))),
    )

    def repr_expr(expr):
        for match, repr_func in expr_kinds:
            if expr == match:
                return repr_func(expr)
        assert False, 'Invalid linearised IR expression: {}'.format(expr)

    def repr_std_stmt(mnemonic, fields):
        def repr_func(stmt):
            return '    {}{}'.format(mnemonic.ljust(6), ', '.join(
                repr_expr(getattr(stmt, field))
                for field in fields.split()))
        return repr_func

    def repr_cjump(cjump):
        return '    cjump {} {} {}, {}'.format(
            repr_expr(cjump.left), relops[cjump.op], repr_expr(cjump.right),
            cjump.lbtrue.name)

    opcodes = (
        # (statement match, repr)
        (WhenMove(), repr_std_stmt('mov', 'dst src')),
        (WhenExpr(), repr_std_stmt('expr', 'expr')),
        (WhenJump(), repr_std_stmt('jump', 'dst')),
        (WhenCJump(), repr_cjump),
        (WhenLabel(), lambda label: '{}:'.format(label.label.name)),
    )
    for stmt in stmts:
        for match, repr_func in opcodes:
            if stmt == match:
                print(repr_func(stmt))
                break
        else:
            assert False, 'Invalid linearised IR statement: {}'.format(stmt)


def unzip(l, fields):
    '''
    Reverse the effect of the builtin zip() function.

    >>> unzip([(1, 2), (3, 4), (5, 6)], 2)
    ([1, 3, 5], [2, 4, 6])
    '''
    result = tuple([] for i in range(fields))
    for row in l:
        for i, cell in enumerate(row):
            result[i].append(cell)
    return result
