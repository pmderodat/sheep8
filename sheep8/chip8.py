from sheep8 import program
from sheep8.frame import (
    BaseFrame, InFrame, InReg, in_frame, in_register, Reg)
from sheep8.translate import Const, Name, Temp, Mem, BinOp, PLUS
from sheep8.utils import align, record


ProcFrag, WhenProcFrag = record('ProcessFragment', 'body frame')
SpriteFrag, WhenSpriteFrag = record('SpriteFragment', 'label data')


class Chip8Frame(BaseFrame):

    registers = {
        'v{}'.format(i): Reg('v{}'.format(i))
        for i in '0123456789abcdef'}

    create_proc_fragment = ProcFrag

    # No variable can escape since there is no pointer nor variable passing.

    @classmethod
    def map_proc(cls, frag, func):
        if frag == WhenProcFrag():
            frag.body = func(frag.body, frag.frame)

    def __init__(self, frame_no, name):
        super(Chip8Frame, self).__init__(frame_no, name)

        self.word_size = 1

        # Formals can only take the 4 first registers (v0 to v3)
        self.formals_in_regs = 0
        self.formals_frame_offset = 0

        self.locals = []
        self.locals_size = 0

        self._frame_ptr = Name(program.Label(
            'funFP{}_{}'.format(frame_no, name)))
        self._return_val = Temp(self.registers['v0'])

        self.formals = []

    def _add_formal(self, type, escapes):
        # Chip8 does not support pointers nor nested function scopes, and thus
        # do not handle formals/locals that escape.
        assert not escapes, 'Formals cannot escape in Chip8'
        if type.size <= self.word_size:
            location = InReg(self.create_temp())
            self.formals_in_regs += 1
        else:
            location = InFrame(self.formals_frame_offset, self)
            self.formals_frame_offset += self.word_size
        self.formals.append(type)
        return location

    def add_local(self, type, escapes):
        assert not escapes, 'Locals cannot escape in Chip8'
        self.locals.append(type)
        self.locals_size += align(type.size, self.word_size)
        return InReg(self.create_temp())

    def get_access(self, storage):
        if storage == in_frame:
            return Mem(BinOp(
                PLUS,
                self.get_frame_ptr(), Const(storage.shift)))
        elif storage == in_register:
            return Temp(storage.temp)
        else:
            assert False, 'Invalid storage type: {}'.format(storage)

    def get_frame_ptr(self):
        return self._frame_ptr

    def get_return_val(self):
        return self._return_val

    def add_view_shift(self, body):
        # TODO: implement view shift: move callee-save registers to frame at
        # the beginning, restore them at the end
        return body
