from sheep8.program import Label
from sheep8.utils import record


InFrame, WhenInFrame = record('InFrame', 'shift frame')
InReg, WhenInReg = record('InRegister', 'temp')

in_frame = WhenInFrame()
in_register = WhenInReg()

# Specific architecture register (not abstract one like a temporary)
Reg, WhenReg = record('Register', 'name')


class BaseFrame:

    registers = None
    create_proc_fragment = None

    @classmethod
    def map_proc(cls, frag, func):
        '''
        Map the body of each function fragment with the given function.
        '''
        raise NotImplementedError()

    def __init__(self, frame_no, name):
        self.frame_no = frame_no
        self.name = name

        self.label_start = Label('fun{}_{}'.format(frame_no, name))
        self.label_end = Label('funEnd{}_{}'.format(frame_no, name))

        self.formals_locations = []

    def add_formal(self, type, escapes):
        location = self._add_formal(type, escapes)
        self.formals_locations.append(location)
        return location

    def _add_formal(self, type, escapes):
        raise NotImplementedError()

    def add_local(self, type, escapes):
        raise NotImplementedError()

    def get_access(self, storage):
        raise NotImplementedError()

    def get_frame_ptr(self):
        raise NotImplementedError()

    def get_return_val(self):
        raise NotImplementedError()

    def add_view_shift(self, body):
        raise NotImplementedError()
