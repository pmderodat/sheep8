from sheep8.utils import record


class Environ:

    def __init__(self, parent=None):
        self.table = {}
        self.parent = parent

    def lookup(self, name):
        try:
            return self.table[name]
        except KeyError:
            if not self.parent:
                raise
        return self.parent.lookup(name)

    def define(self, entry):
        if self.table.setdefault(entry.name, entry) is not entry:
            raise KeyError('Already defined entry: {}'.format(entry.name))


VarSym, WhenVarSym = record('VariableSymbol', 'location name type storage')
FunSym, WhenFunSym = record('FunctionSymbol',
    'location name rettype argtypes label')
