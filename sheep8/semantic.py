from collections import defaultdict

from sheep8 import ast
from sheep8.environ import Environ, VarSym, FunSym, WhenVarSym, WhenFunSym
from sheep8.errors import CompilationError, IncompatibleTypeError
from sheep8.pervasives import get_pervasive_types, get_pervasive_symbols
from sheep8.program import Program
from sheep8.translate import Translator
from sheep8.types import (
    TypeEnviron, void, byte,
    can_assign, get_binop_type, get_preop_type
)
from sheep8.utils import format_location, unzip


var_sym = WhenVarSym()
func_sym = WhenFunSym()
main_type = WhenFunSym(name='main', rettype=void, argtypes=[])

expr_ident = ast.WhenIdent()
expr_int = ast.WhenInt()
expr_preop = ast.WhenPreOp()
expr_binop = ast.WhenBinOp()
expr_call = ast.WhenCall()

stmt_expr = ast.WhenExpr()
stmt_var_decl = ast.WhenVarDecl()
stmt_affect = ast.WhenAffect()
stmt_if = ast.WhenIf()
stmt_ifalt = ast.WhenIfAlt()
stmt_while = ast.WhenWhile()
stmt_do_while = ast.WhenDoWhile()
stmt_return = ast.WhenRet()


class Semantic:
    '''
    Gather program and translator alongside with methods to check the semantic
    of an AST and to translate it to an IR tree.
    Each method takes a kind of AST node, a symbol environment and a type
    environment and return the node IR translation plus optionnal results.
    '''

    def __init__(self, frame_type):
        self.program = Program(frame_type)
        self.translator = Translator(self.program)
        self.frame_to_symbol = {}

        # Graph used to detect loops:
        #   function name -> set of directly called functions' names
        # Ex: if func1 calls func2 and if func2 calls func3, then
        #   self.call_graph['func1'] = {'func2': call_location}
        #   self.call_graph['func2'] = {'func3': call_location}
        self.call_graph = defaultdict(dict)

    @staticmethod
    def is_lvalue(expr):
        '''
        Return if the given AST expression node is a l-value.
        '''
        if expr == expr_ident:
            return True
        elif expr in (expr_int, expr_preop, expr_binop, expr_call):
            return False
        else:
            assert False, 'Invalid expression: {}'.format(expr)

    def register_call(self, caller_sym, callee_sym, call_location):
        '''
        Register a call to maintain a call graph.
        Raise a CompilationError if a loop is detected.
        '''
        caller, callee = caller_sym.name, callee_sym.name
        if caller == callee:
            raise CompilationError(
                'Recursion is not allowed: {} calls itself'.format(caller),
                call_location)

        def check_calls(call_stack):
            first_caller = call_stack[0]
            last_caller = call_stack[-1]
            callees = self.call_graph[last_caller]
            if first_caller in callees:
                long_chain = ''
                if len(call_stack) > 2:
                    long_chain = ', but:{}'.format(''.join(
                        '\n    {}: {} calls {}'.format(
                        format_location(self.call_graph[caller][call_stack[i+1]]),
                        caller, call_stack[i + 1])
                        for i, caller in enumerate(call_stack[1:-1], 1)))
                raise CompilationError(
                    'Recursion is not allowed: {} calls {}{}'.format(
                        call_stack[0],
                        (
                            'itself'
                            if call_stack[0] == call_stack[1] else
                            call_stack[1]
                        ),
                        long_chain),
                    call_location)
            for callee in callees:
                call_stack.append(callee)
                check_calls(call_stack)
                call_stack.pop()

        # Update the graph: one function can call many times one other in its
        # body. Just keep in mind the location of the first call.
        self.call_graph[caller].setdefault(callee, call_location)
        check_calls([caller])

    def process_cond(self, expr, frame, symbols, types):
        '''
        Process semantic analysis on an expression that is used as a condition
        and return IR translation an computed type, or raise a
        CompilationError.
        '''
        cond_ir, cond_type = self.process_expr(expr, frame, symbols, types)
        if cond_type != byte:
            raise CompilationError(
                'A byte is required for the condition'
                ' ({} given)'.format(
                    cond_type),
                expr.location)
        return (cond_ir, cond_type)

    def process_expr(self, expr, frame, symbols, types):
        '''
        Process semantic analysis on an expression and return IR translation
        and computed type, or raise a CompilationError.
        '''
        if expr == expr_ident:
            try:
                sym = symbols.lookup(expr.name)
            except KeyError:
                raise CompilationError(
                    'Undefined variable: {}'.format(expr.name), expr.location)
            if sym == func_sym:
                raise CompilationError(
                    'Functions are not first-class values,'
                    ' you can only call them',
                    expr.location)
            elif sym == var_sym:
                return (self.translator.trans_ident(sym, frame), sym.type)
            else:
                assert False, (
                    'Something that is not a function nor a variable is in the'
                    'symbol environment: {}'.format(sym))
        elif expr == expr_int:
            # Right now, only unsigned bytes are handled
            if expr.value < 0:
                raise CompilationError('Negative bytes are not handled',
                    expr.location)
            elif expr.value > 255:
                raise CompilationError(
                    '{} cannot fit in one byte'.format(expr.value),
                    expr.location)
            return (self.translator.trans_byte(expr), byte)

        elif expr == expr_preop:
            expr_ir, expr_type = self.process_expr(
                expr.expr, frame, symbols, types)
            try:
                result_type = get_preop_type(expr.op, expr_type)
            except IncompatibleTypeError as e:
                raise e.to_external(expr.location)
            return (self.translator.trans_preop(expr.op, expr_ir), result_type)
        elif expr == expr_binop:
            l_ir, l_type = self.process_expr(expr.left, frame, symbols, types)
            r_ir, r_type = self.process_expr(expr.right, frame, symbols, types)
            try:
                e_type = get_binop_type(expr.op, l_type, r_type)
            except IncompatibleTypeError as e:
                raise e.to_external(expr.location)
            # TODO: check for multiplication. Target architecture does not
            # handle multiplication, thus those operations must be computed at
            # compile-time (expect for powers of two).
            return (self.translator.trans_binop(expr.op, l_ir, r_ir), e_type)

        elif expr == expr_call:
            try:
                sym = symbols.lookup(expr.name)
            except KeyError:
                raise CompilationError(
                    'Undefined function: {}'.format(expr.name), expr.location)
            if sym == var_sym:
                raise CompilationError(
                    'You cannot call a variable, only functions', expr.location)
            elif sym == func_sym:
                self.register_call(
                    self.frame_to_symbol[id(frame)], sym,
                    expr.location)
                ir_args, arg_types = unzip(
                    (
                        self.process_expr(arg, frame, symbols, types)
                        for arg in expr.args),
                    2)
                if len(arg_types) != len(sym.argtypes):
                    raise CompilationError(
                        '{} requires {} arguments ({} given)'.format(
                            sym.name, len(sym.argtypes), len(arg_types)))
                for i, (given_type, dest_type) in enumerate(
                        zip(arg_types, sym.argtypes), 1):
                    if not can_assign(dest_type, given_type):
                        raise CompilationError(
                            '{} requires a {} for argument {}'
                            ' ({} given)'.format(
                                sym.name, dest_type, i, given_type),
                            expr.location)
                return (
                    self.translator.trans_call(sym, ir_args),
                    sym.rettype)

        else:
            assert False, 'Invalid expression: {}'.format(expr)

    def process_stmt(self, stmt, frame, symbols, types):
        '''
        Process semantic analysis on a statement. Can raise a CompilationError
        when needed.
        '''
        if stmt == stmt_var_decl:
            var_type = types.canonize(stmt.type)
            if var_type == void:
                raise CompilationError(
                    'Variables cannot be void', stmt.location)
            ir_node, expr_type = self.process_expr(
                stmt.expr, frame, symbols, types)
            if not can_assign(var_type, expr_type):
                raise CompilationError(
                    'Cannot assign a {} to a {} variable'.format(
                        expr_type, var_type),
                    stmt.location)
            sym = VarSym(stmt.location,
                stmt.name, var_type,
                frame.add_local(var_type, False))
            try:
                symbols.define(sym)
            except KeyError:
                prev_sym = symbols.lookup(stmt.name)
                raise CompilationError(
                    'Already defined variable {} (at {})'.format(
                        prev_sym.name, format_location(prev_sym.location)),
                    stmt.location)

            return self.translator.trans_vardecl(sym, ir_node, frame)

        elif stmt == stmt_affect:
            if not self.is_lvalue(stmt.left):
                raise CompilationError(
                    'Target is not a l-value', stmt.location)
            l_ir, lval_type = self.process_expr(
                stmt.left, frame, symbols, types)
            r_ir, rval_type = self.process_expr(
                stmt.right, frame, symbols, types)
            if not can_assign(lval_type, rval_type):
                raise CompilationError(
                    'Cannot assign a {} to a {} variable'.format(
                        rval_type, lval_type),
                    stmt.location)
            return self.translator.trans_affect(l_ir, r_ir)

        elif stmt == stmt_if:
            alts = []
            result_type = None
            for alt in stmt.alts:
                cond_ir, cond_type = self.process_cond(
                    alt.cond, frame, symbols, types)
                body_ir = self.process_block(alt.body, frame, symbols, types)
                alts.append((cond_ir, body_ir))
            default_ir = self.process_block(
                stmt.default, frame, symbols, types)
            return self.translator.trans_if(alts, default_ir)

        elif stmt in (stmt_while, stmt_do_while):
            cond_ir, cond_type = self.process_cond(
                stmt.cond, frame, symbols, types)
            body_ir = self.process_block(stmt.body, frame, symbols, types)
            return self.translator.trans_while(
                cond_ir, body_ir, stmt == stmt_do_while)

        elif stmt == stmt_return:
            ir_node, given_type = self.process_expr(
                stmt.expr, frame, symbols, types)
            func_sym = types.get_function()
            if not can_assign(func_sym.rettype, given_type):
                raise CompilationError(
                    'Cannot return a {}, {} expects a {} instead'.format(
                        given_type, func_sym.name, func_sym.rettype),
                    stmt.location)
            return self.translator.trans_return(ir_node, frame)

        elif stmt == stmt_expr:
            # The value of this expression may be used if this statement is the
            # last one of a conditionnal block.
            ir_node, expr_type = self.process_expr(
                stmt.expr, frame, symbols, types)
            # This may be the ony time we annotate an AST node during the
            # semantic pass.
            return self.translator.trans_expr(ir_node)
        else:
            assert False, 'Invalid statement: {}'.format(stmt)

    def process_block(self, block, frame, symbols, types):
        '''
        Process a list of instructions. Return IR nodes and the type of the
        last statement (which is void unless last statement is an Expr). The
        returned IR node is an expression.
        '''
        return self.translator.trans_block([
            self.process_stmt(stmt, frame, symbols, types)
            for stmt in block
        ])

    def process_func(self, func, frame, symbols, types):
        # First add arguments to top-most environment
        for arg in func.args:
            arg_type = types.canonize(arg.type)
            if arg_type == void:
                raise CompilationError(
                    'Function arguments cannot be void', arg.location
                )
            sym = VarSym(arg.location,
                arg.name, arg_type,
                frame.add_formal(arg_type, False))
            try:
                symbols.define(sym)
            except KeyError:
                prev_sym = symbols.lookup(arg.name)
                raise CompilationError(
                    'Already defined symbol {} (at {})'.format(
                        arg.name, format_location(prev_sym.location)),
                    arg.location)

        local_symbols = Environ(symbols)
        local_types = TypeEnviron(types)
        # Then add local variables to local environment and check symbol usages
        ir_stmts = self.process_block(
            func.body, frame, local_symbols, local_types)
        return self.translator.trans_func(ir_stmts, frame)

    def process(self, tree):
        frames = {}
        symbols = get_pervasive_symbols()
        types = get_pervasive_types()

        self.program.tree = tree
        self.program.symbols = symbols
        self.program.types = types

        # The first pass is only used to register function names and arguments
        for node in tree:
            if node == ast.WhenFunc():
                frame = self.program.create_frame(node.name)
                symbol = FunSym(node.location, node.name,
                    types.canonize(node.rettype),
                    [types.canonize(arg.type) for arg in node.args],
                    frame.label_start)
                try:
                    symbols.define(symbol)
                except KeyError:
                    prev_func = symbols.lookup(node.name)
                    raise CompilationError(
                        'Already defined symbol {} (at {})'.format(
                            node.name, format_location(prev_func.location)),
                        node.location)
                frames[node.name] = frame
                self.frame_to_symbol[id(frame)] = symbol

        # Every program must have a `main() void` entry point. If not, raise an
        # error.
        try:
            main_sym = symbols.lookup('main')
        except KeyError:
            raise CompilationError(
                'An entry point (`main` function) is missing', None)
        if main_sym != main_type:
            raise CompilationError(
                'Entry point must have no argument and must return nothing (void)',
                main_sym.location)

        for node in tree:
            if node == ast.WhenFunc():
                sym = symbols.lookup(node.name)
                local_symbols = Environ(symbols)
                local_types = TypeEnviron(types, sym)
                frame = frames[node.name]
                ir_body = self.process_func(
                    node, frame, local_symbols, local_types)
                self.program.add_fragment(
                    frame.create_proc_fragment(ir_body, frame))
            else:
                assert False, 'Invalid AST node at top level: {}'.format(node)

        return self.program
