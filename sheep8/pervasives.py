from sheep8.environ import Environ, VarSym, FunSym
from sheep8.types import TypeEnviron, void, byte


def get_pervasive_symbols():
    # There are no pervasives right now...
    return Environ()

def get_pervasive_types():
    result = TypeEnviron()
    result.define(void)
    result.define(byte)
    return result
