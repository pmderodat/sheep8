Sheep8 - Chip-8 specific toy DSL compiler
=========================================

Sheep8 is both a DSL and the associated compiler. Its target is to enable one
to use a high level language to write games for the Chip-8 platform.

Though Chip-8 is not a very used technology, I started this project to teach
myself how to write a compiler, using “Modern Compiler Implementation” (Andrew
W. Appel).

Chip-8 is a powerless architecture that, for instance, makes it hard to do
random (code and data) memory access. Language design/compiler creation is thus
a challenge for me and requires to think more than once about it.

Thus, this is a work-in-progress: I am currently implementing a very basic (and
useless) language compiler as my first “engine” and I will add some features in
order to actually be able to create some game. ;-)
